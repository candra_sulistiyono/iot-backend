from flask import Flask, flash, render_template, g, request,  jsonify, url_for, redirect
from pymongo import MongoClient
from uuid import uuid4
from modul import setting, history, user, logger, images, wifi, helmed
import flask_admin
from flask_admin import helpers as admin_helpers, BaseView
import os,json
from datetime import datetime

from flask_mongoengine import MongoEngine
from flask_security import Security, MongoEngineUserDatastore, \
    UserMixin, RoleMixin, login_required
from flask_principal import Permission, RoleNeed
#from flask_cors import CORS, cross_origin
import logging
from logging.handlers import RotatingFileHandler


#from apscheduler.schedulers.background import BackgroundScheduler

hostMongo='localhost'
mongoClient = MongoClient(hostMongo, 27017)
db = mongoClient.iot
envSetting = os.environ['CAMERA_SYS_ENV']
urlServer = "localhost:7201"

PORT_SERVICE = 7201
ResponseHttp = {}

app = Flask(__name__)
if envSetting=='DEVELOPMENT':
    hostMongo='172.27.3.11'
    urlServer="http://iot-backend-dev.promogo.com"
    mongoClient = MongoClient(hostMongo, 27017)
    db = mongoClient.iot
from flask_cors import CORS, cross_origin
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

app.config.from_pyfile('config.py')
db = MongoEngine(app)
admin_permission = Permission(RoleNeed('superuser'))

class Role(db.Document, RoleMixin):
    name = db.StringField(max_length=80, unique=True)
    description = db.StringField(max_length=255)
class User(db.Document, UserMixin):
    user = db.StringField(max_length=255)
    password = db.StringField(max_length=255)
    email = db.StringField(max_length=255)
    name = db.StringField(max_length=255)
    active = db.BooleanField(default=True)
    confirmed_at = db.DateTimeField()
    created_at = db.DateTimeField()
    roles = db.ListField(db.ReferenceField(Role), default=[])
    token = db.StringField(max_length=255)
    chat_id = db.StringField(max_length=255)
    merchant_id = db.IntField()


user_datastore = MongoEngineUserDatastore(db, User, Role)
security = Security(app, user_datastore)


@app.route('/')
def index():
    return render_template('index.html')

@app.route('/pushregister',methods = ['POST', 'GET'])
def result():
    if request.method == 'POST':
        superuser_role = user_datastore.find_or_create_role('superuser')
        password = request.form['password']
        passwordConfirm = request.form['password_confirm']
        name = request.form['name']
        email = request.form['email']
        roles = [superuser_role]
        active = False
        token = str(uuid4())
        chatId = str(uuid4())
        merchantId = 70
        createdAt = str(datetime.now())
        if (password == passwordConfirm):
            user_datastore.create_user(
                password=password, name=name, email=email,
                roles=roles, token=token, active=active, chat_id=chatId, created_at=createdAt, merchant_id=merchantId)
            flash('Thanks for registration, ' + name)
            return redirect("/admin/")
        else :
            flash('Input password not same ')
            return redirect("/register")
    else:
        return redirect("/register")

@app.route('/admin')
def get_admin():
    flash('Input password not same ')
    return redirect("/admin/")

admin = flask_admin.Admin(
    app,
    'Dashboard Example',
    base_template='my_master.html',
    template_mode='bootstrap3',
)

# define a context processor for merging flask-admin's template context into the
# flask-security views.
@security.context_processor
def security_context_processor():
    return dict(
        admin_base_template=admin.base_template,
        admin_view=admin.index_view,
        h=admin_helpers,
        get_url=url_for
    )



@app.before_request
def before_request():
    try:
        mongoClient.connect
    except :
        logger.save("No database connection could be established.")


@app.teardown_request
def teardown_request(exception):
    try:
        mongoClient.close()
    except AttributeError:
        pass

# ********** history ***********
@app.route("/history/list",methods=["POST"])
def history_list():
    #print(str(request.headers))
    try:
        #print(request.headers['token'])
        #if (user.cek_user_token(request.headers['token'])):
            #data=request.json
            ResponseHttp['code'] = 200
            ResponseHttp['message'] = 'ok'
            page = 1
            limit = 27
            idCar = ""
            #if 'page' in data:
            #    page = data['page']
            #if 'limit' in data:
            #    limit = data['limit']
            #if 'id_car' in data:
            #    idCar = data['id_car']
            ResponseHttp['data'] = history.history_list(page,limit,idCar)
        #else:
        #    ResponseHttp['code'] = 401
        #    ResponseHttp['message'] = 'Not Authorized'
        #    ResponseHttp['data'] = []
    except Exception as e:
        logger.save('Failed history_list() ', str(e))
        ResponseHttp['code'] = 400
        ResponseHttp['message'] = 'Bad Request'
        ResponseHttp['data'] = []

    response = app.response_class(
        response=json.dumps(ResponseHttp),
        status=ResponseHttp['code'],
        mimetype='application/json'
    )
    return json.dumps(ResponseHttp) #response

@app.route("/history/rekap",methods=["GET"])
def history_rekap1():
    try:
        if (user.cek_user_token(request.headers['token'])):
            ResponseHttp['code'] = 200
            ResponseHttp['message'] = 'ok'
            ResponseHttp['data'] = history.history_rekap1()
        else:
            ResponseHttp['code'] = 401
            ResponseHttp['message'] = 'Not Authorized'
            ResponseHttp['data'] = []
    except Exception as e:
        logger.save('Failed history_list() ', str(e))
        ResponseHttp['code'] = 400
        ResponseHttp['message'] = 'Bad Request'
        ResponseHttp['data'] = []

    return json.dumps(ResponseHttp),ResponseHttp['code']
@app.route("/history/rekap2",methods=["GET"])
def history_rekap2():
    try:
        if (user.cek_user_token(request.headers['token'])):
            ResponseHttp['code'] = 200
            ResponseHttp['message'] = 'ok'
            ResponseHttp['data'] = history.history_rekap2()
        else:
            ResponseHttp['code'] = 401
            ResponseHttp['message'] = 'Not Authorized'
            ResponseHttp['data'] = []
    except Exception as e:
        logger.save('Failed history_list() ', str(e))
        ResponseHttp['code'] = 400
        ResponseHttp['message'] = 'Bad Request'
        ResponseHttp['data'] = []

    return jsonify(ResponseHttp),ResponseHttp['code']

@app.route("/server/status",methods=["POST"])
def server_status():
    #print(str(request.headers))
    try:
        #print(request.headers['token'])
        #if (user.cek_user_token(request.headers['token'])):
            #data=request.json
            ResponseHttp['code'] = 200
            ResponseHttp['message'] = 'ok'
            ResponseHttp['data'] = []
            logger.save('server_status() ', str(datetime.now()))
        #else:
        #    ResponseHttp['code'] = 401
        #    ResponseHttp['message'] = 'Not Authorized'
        #    ResponseHttp['data'] = []
    except Exception as e:
        logger.save('Failed server_status() ', e)
        ResponseHttp['code'] = 400
        ResponseHttp['message'] = 'Bad Request'
        ResponseHttp['data'] = []

    response = app.response_class(
        response=json.dumps(ResponseHttp),
        status=ResponseHttp['code'],
        mimetype='application/json'
    )
    return response

@app.route("/update/status",methods=["POST"])
def update_status():
    updateStatus = setting.get_setting_value('Update')
    #print(str(request.headers))
    try:
        #print(request.headers['token'])
        #if (user.cek_user_token(request.headers['token'])):
            #data=request.json
            ResponseHttp['code'] = 200
            ResponseHttp['message'] = 'ok'
            ResponseHttp['data'] = updateStatus
            logger.save('update_status() ', str(datetime.now()))
            setting.setting_update('Update',0)
        #else:
        #    ResponseHttp['code'] = 401
        #    ResponseHttp['message'] = 'Not Authorized'
        #    ResponseHttp['data'] = []
    except Exception as e:
        logger.save('Failed server_status() ', e)
        ResponseHttp['code'] = 400
        ResponseHttp['message'] = 'Bad Request'
        ResponseHttp['data'] = 0

    response = app.response_class(
        response=json.dumps(ResponseHttp),
        status=ResponseHttp['code'],
        mimetype='application/json'
    )
    return response

@app.route("/history/save",methods=["POST"])
def history_save():
    token=request.headers['token']
    try:
        if (user.cek_user_token(token)):
            ResponseHttp['code'] = 201
            ResponseHttp['message'] = 'History Saved...'
            id=''
            updateBy=''
            #mode 1 -> Update
            if request.json['mode']==1:
                id=request.json['id']
                updateBy = request.json['update_by']
            ResponseHttp['data'] = history.history_save(request.json['mode'], id, token,
                                                        request.json['list_data'],
                                                        updateBy)
            if ResponseHttp['data'] != 'error':
                ResponseHttp['status']=1  #Success
            else :
                ResponseHttp['status']=0
            #ResponseHttp['data'] = history.history_save(request.json['mode'], id,token,
            #                                                       request.json['cars'], request.json['motors'],request.json['plates'],
            #                                                       request.json['humans'],request.json['lat'], request.json['long'],
            #                                                       updateBy)
        else:
            ResponseHttp['code'] = 401
            ResponseHttp['message'] = 'Not Authorized'
            ResponseHttp['data'] = []
    except Exception as e:
        logger.save('error on history_save() => ', e)
        ResponseHttp['code'] = 400
        ResponseHttp['message'] = 'Bad Request'
        ResponseHttp['data'] = []
    return jsonify(ResponseHttp)

@app.route("/history/save2",methods=["POST"])
def history_save2():
    token=request.headers['token']
    try:
        if (user.cek_user_token(token)):
            ResponseHttp['code'] = 201
            ResponseHttp['message'] = 'History Saved...'
            id=''
            updateBy=''
            #mode 1 -> Update
            if request.json['mode']==1:
                id=request.json['id']
                updateBy = request.json['update_by']
            ResponseHttp['data'] = history.history_save2(request.json['mode'], id, token,
                                                        request.json['list_data'],
                                                        updateBy)
            if ResponseHttp['data'] != 'error':
                ResponseHttp['status']=1  #Success
            else :
                ResponseHttp['status']=0
            #ResponseHttp['data'] = history.history_save(request.json['mode'], id,token,
            #                                                       request.json['cars'], request.json['motors'],request.json['plates'],
            #                                                       request.json['humans'],request.json['lat'], request.json['long'],
            #                                                       updateBy)
        else:
            ResponseHttp['code'] = 401
            ResponseHttp['message'] = 'Not Authorized'
            ResponseHttp['data'] = []
    except Exception as e:
        logger.save('error on history_save() => ', e)
        ResponseHttp['code'] = 400
        ResponseHttp['message'] = 'Bad Request'
        ResponseHttp['data'] = []
    return jsonify(ResponseHttp)
@app.route("/image/save",methods=["POST"])
def image_save():
    token=request.headers['token']
    try:
        if (user.cek_user_token(token)):
            ResponseHttp['code'] = 201
            ResponseHttp['message'] = 'image Saved...'
            ResponseHttp['data'] = []
            file =request.files['image']
            file.save("static/imgs/"+file.filename)
            #baseUrl=request.base_url
            #baseUrl=baseUrl[0:baseUrl.find('image')]+"static/imgs/"+file.filename
            baseUrl = urlServer + "/static/imgs/" + file.filename

            images.images_save(0, '', token,request.form['camera'],file.filename,baseUrl, 'system')
        else:
            ResponseHttp['code'] = 401
            ResponseHttp['message'] = 'Not Authorized'
            ResponseHttp['data'] = []
    except Exception as e:
        logger.save('error on image_save() => ', e)
        ResponseHttp['code'] = 400
        ResponseHttp['message'] = 'Bad Request'
        ResponseHttp['data'] = []
    return jsonify(ResponseHttp),ResponseHttp['code']

@app.route("/image/save2",methods=["POST"])
def image_save2():
    token=request.headers['token']
    try:
        if (user.cek_user_token(token)):
            ResponseHttp['code'] = 201
            ResponseHttp['message'] = 'image Saved...'
            ResponseHttp['data'] = []
            file =request.files['image']
            file.save("static/imgs/"+file.filename)
            #baseUrl=request.base_url
            #baseUrl=baseUrl[0:baseUrl.find('image')]+"static/imgs/"+file.filename
            baseUrl = urlServer + "/static/imgs/" + file.filename

            images.images_save(0, '', token,request.form['camera'],file.filename,baseUrl, 'system')
        else:
            ResponseHttp['code'] = 401
            ResponseHttp['message'] = 'Not Authorized'
            ResponseHttp['data'] = []
    except Exception as e:
        logger.save('error on image_save() => ', e)
        ResponseHttp['code'] = 400
        ResponseHttp['message'] = 'Bad Request'
        ResponseHttp['data'] = []
    return jsonify(ResponseHttp),ResponseHttp['code']
@app.route("/images/list")
def images_list():
    try:
        if (user.cek_user_token(request.headers['token'])):
            ResponseHttp['code'] = 200
            ResponseHttp['message'] = 'ok'
            page = request.args.get('page', default=1, type=int)
            limit = request.args.get('limit', default=10, type=int)
            idCar = request.headers['token'] #request.args.get('id_car', default='', type=str)
            ResponseHttp['data'] = images.images_list(page,limit,'')
        else:
            ResponseHttp['code'] = 401
            ResponseHttp['message'] = 'Not Authorized'
            ResponseHttp['data'] = []
    except Exception as e:
        logger.save('Failed history_list() ', str(e))
        ResponseHttp['code'] = 400
        ResponseHttp['message'] = 'Bad Request'
        ResponseHttp['data'] = []

    return jsonify(ResponseHttp),ResponseHttp['code']


@app.route("/logger/save",methods=["POST"])
def logger_save():
    token=request.headers['token']
    ResponseHttp['data']=[]
    try:
        if (user.cek_user_token(token)):
            ResponseHttp['code'] = 201
            ResponseHttp['message'] = logger.save_from_other(request.json['engine'],request.json['endPoint'],request.json['message'])
        else:
            ResponseHttp['code'] = 401
            ResponseHttp['message'] = 'Not Authorized'
    except Exception as e:
        logger.save('error on history_save() => ', str(e))
        ResponseHttp['code'] = 400
        ResponseHttp['message'] = 'Bad Request'
    return jsonify(ResponseHttp)

@app.route("/history/clear",methods=[ 'POST'])
def history_clear():
    try:
        if (user.cek_user_token(request.json['token'])):
            ResponseHttp['code'] = 200
            ResponseHttp['data'] = []

            if history.history_clear():
                ResponseHttp['message'] = 'History Cleared'
            else:
                ResponseHttp['message'] = 'History Cleared Failed'
        else:
            ResponseHttp['code'] = 401
            ResponseHttp['message'] = 'Not Authorized'
            ResponseHttp['data'] = []
    except Exception as e:
        logger.save('error on History clear => ',str(e))
        ResponseHttp['code'] = 400
        ResponseHttp['message'] = 'Bad Request'
    return jsonify(ResponseHttp)

@app.route("/history/<id>/delete",methods=["DELETE"])
def history_delete(id):
    try:
        if (user.cek_user_token(request.headers['token'])):
            ResponseHttp['code'] = 200
            ResponseHttp['data'] = []
            if history.history_delete(id):
                ResponseHttp['message'] = 'Delete History Success'
            else:
                ResponseHttp['message'] = 'Delete History failed'
        else:
            ResponseHttp['code'] = 401
            ResponseHttp['message'] = 'Not Authorized'
            ResponseHttp['data'] = []
    except:
        ResponseHttp['code'] = 400
        ResponseHttp['message'] = 'Bad Request'
        ResponseHttp['data'] = []
    return jsonify(ResponseHttp)

@app.route("/history/<id>/view",methods=["GET"])
def history_view(id):
    try:
        if (user.cek_user_token(request.headers['token'])):
            ResponseHttp['code'] = 200
            ResponseHttp['message'] = 'ok'
            ResponseHttp['data'] = history.history_view(id)
        else:
            ResponseHttp['code'] = 401
            ResponseHttp['message'] = 'Not Authorized'
            ResponseHttp['data'] = []
    except:
        ResponseHttp['code'] = 400
        ResponseHttp['message'] = 'Bad Request'
        ResponseHttp['data'] = []
    return jsonify(ResponseHttp)

@app.route("/history/report/day",methods=[ 'GET'])
def history_report_day():
    try:
        if (user.cek_user_token(request.headers['token'])):
            ResponseHttp['code'] = 200
            ResponseHttp['message'] = 'ok'
            page = request.args.get('page', default=1, type=int)
            limit = request.args.get('limit', default=10, type=int)
            idCar = request.headers['token'] #request.args.get('id_car', default='', type=str)
            ResponseHttp['data'] = history.history_list(page,limit,idCar)
        else:
            ResponseHttp['code'] = 401
            ResponseHttp['message'] = 'Not Authorized'
            ResponseHttp['data'] = []
    except Exception as e:
        logger.save('Failed history_list() ', str(e))
        ResponseHttp['code'] = 400
        ResponseHttp['message'] = 'Bad Request'
        ResponseHttp['data'] = []
    #print(json.dumps(ResponseHttp))
    return jsonify(ResponseHttp),ResponseHttp['code']





@app.teardown_request
def teardown_request(exception):
    try:
        g.rdb_conn.close()
    except AttributeError:
        pass


@app.route("/user",methods=[ 'GET'])
def user_index():
    page = request.args.get('page', default=1, type=int)
    limit = request.args.get('limit', default=10, type=int)
    searchText = request.args.get('search', default='', type=str)
    try:
        listData = user.user_list_query(page, limit, searchText)
        countData = len(listData)
        return UserView().render('user/index.html', datas=listData, page=page, limit=limit, searchText=searchText,
                                 countData=countData)
    except Exception as e:
        logger.save('Failed user_index() ' , str(e))
        ResponseHttp['code'] = 400
        ResponseHttp['message'] = 'Bad Request'
        ResponseHttp['data'] = []
    return jsonify(ResponseHttp)

@app.route("/user/<id>/update")
def user_update(id):
    try:
        page = request.args.get('page', default=1, type=int)
        limit = request.args.get('limit', default=10, type=int)
        searchText = request.args.get('search', default='', type=str)
        data = user.user_view(id)
        listRole = user.role_list()
        return UserUpdate().render('user/form.html', mode='UPDATE', data=data, listRole=listRole, page=page, limit=limit, searchText=searchText)
    except Exception as e:
        logger.save('Failed user_update() ' , str(e))
        ResponseHttp['code'] = 400
        ResponseHttp['message'] = 'Bad Request'
        ResponseHttp['data'] = []
        return jsonify(ResponseHttp)

@app.route("/user/<id>/delete")
def user_delete(id):
    try:
        user.user_delete(id)
        return redirect("/user")
    except Exception as e:
        logger.save('Failed user_update() ' , str(e))
        ResponseHttp['code'] = 400
        ResponseHttp['message'] = 'Bad Request'
        ResponseHttp['data'] = []
        return jsonify(ResponseHttp)

@app.route("/user/save",methods=["POST"])
def user_save():
    try:
        user.user_update(request.form['_id'],request.form['email'],request.form['name'],request.form['token'],request.form['role'],request.form['active'])
        return redirect("/user")

    except Exception as e:
        logger.save('Failed user_update() ' , str(e))
        ResponseHttp['code'] = 400
        ResponseHttp['message'] = 'Bad Request'
        ResponseHttp['data'] = []
    return jsonify(ResponseHttp)


@app.route("/logger",methods=[ 'POST'])
def api_logger():
    page = request.json['page']
    limit = request.json['limit']
    searchText = request.json['search']
    try:
        listData = logger.logger_list_query(page,limit,searchText)
        ResponseHttp['code'] = 200
        ResponseHttp['message'] = 'ok'
        ResponseHttp['data'] = listData
    except Exception as e:
        logger.save('Failed api_logger() ' , str(e))
        ResponseHttp['code'] = 400
        ResponseHttp['message'] = 'Bad Request'
        ResponseHttp['data'] = []
    return jsonify(ResponseHttp)

@app.route("/ip",methods=["GET"])
def get_ip():
    #session = Session(user='wifi')
    #print(str(request.environ))
    id1 =request.environ['REMOTE_ADDR']
    id2 = request.environ.get('HTTP_X_FORWARDED_FOR', request.remote_addr)

    #return jsonify({'id1': id1, 'id2':id2}),200
    return id2,200
#sched = BackgroundScheduler(daemon=True)
#sched.add_job(job_cron,'interval',days=1)
#sched.start()

@app.route("/wifi/save",methods=["POST"])
def wifi_save():
    token=request.json['token']
    try:
        if (user.cek_user_token(token)):
            idDevice=''
            updateBy=''

            ResponseHttp['data'] = wifi.wifi_save(token,request.json)
            if ResponseHttp['data'] != 'error':
                ResponseHttp['code'] = 200
                ResponseHttp['status']='success'
            else :
                ResponseHttp['status']='failed'
                ResponseHttp['code'] = 500
                ResponseHttp['message'] = 'failed'

        else:
            ResponseHttp['code'] = 401
            ResponseHttp['message'] = 'Not Authorized'
            ResponseHttp['data'] = []
    except Exception as e:
        logger.save('error on history_save() => ', e)
        ResponseHttp['code'] = 400
        ResponseHttp['message'] = 'Bad Request'
        ResponseHttp['data'] = []
    return jsonify(ResponseHttp),ResponseHttp['code']

@app.route("/wifi/report/day",methods=["GET"])
def wifi_report_day():
    try:
        if (user.cek_user_token(request.headers['token'])):
            ResponseHttp['code'] = 200
            ResponseHttp['message'] = 'ok'
            page = request.args.get('page', default=1, type=int)
            limit = request.args.get('limit', default=10, type=int)
            idCar = request.headers['token'] #request.args.get('id_car', default='', type=str)
            ResponseHttp['data'] = wifi.wifi_list(page,limit)
        else:
            ResponseHttp['code'] = 401
            ResponseHttp['message'] = 'Not Authorized'
            ResponseHttp['data'] = []
    except Exception as e:
        logger.save('Failed wifi_list() ', str(e))
        ResponseHttp['code'] = 400
        ResponseHttp['message'] = 'Bad Request'
        ResponseHttp['data'] = []

    return jsonify(ResponseHttp),ResponseHttp['code']
@app.route("/wifi/rekap",methods=["GET"])
def wifi_rekap1():
    try:
        if (user.cek_user_token(request.headers['token'])):
            ResponseHttp['code'] = 200
            ResponseHttp['message'] = 'ok'
            ResponseHttp['data'] = wifi.wifi_rekap1()
        else:
            ResponseHttp['code'] = 401
            ResponseHttp['message'] = 'Not Authorized'
            ResponseHttp['data'] = []
    except Exception as e:
        logger.save('Failed wifi_list() ', str(e))
        ResponseHttp['code'] = 400
        ResponseHttp['message'] = 'Bad Request'
        ResponseHttp['data'] = []

    return json.dumps(ResponseHttp),ResponseHttp['code']
@app.route("/wifi/rekap2",methods=["GET"])
def wifi_rekap2():
    try:
        if (user.cek_user_token(request.headers['token'])):
            ResponseHttp['code'] = 200
            ResponseHttp['message'] = 'ok'
            ResponseHttp['data'] = wifi.wifi_rekap2()
        else:
            ResponseHttp['code'] = 401
            ResponseHttp['message'] = 'Not Authorized'
            ResponseHttp['data'] = []
    except Exception as e:
        logger.save('Failed wifi rekap2() ', str(e))
        ResponseHttp['code'] = 400
        ResponseHttp['message'] = 'Bad Request'
        ResponseHttp['data'] = []

    return jsonify(ResponseHttp),ResponseHttp['code']
@app.route("/wifi/rekap3",methods=["GET"])
def wifi_rekap3():
    try:
        if (user.cek_user_token(request.headers['token'])):
            ResponseHttp['code'] = 200
            ResponseHttp['message'] = 'ok'
            ResponseHttp['data'] = wifi.wifi_rekap3()
        else:
            ResponseHttp['code'] = 401
            ResponseHttp['message'] = 'Not Authorized'
            ResponseHttp['data'] = []
    except Exception as e:
        logger.save('Failed wifi rekap 3t() ', str(e))
        ResponseHttp['code'] = 400
        ResponseHttp['message'] = 'Bad Request'
        ResponseHttp['data'] = []

    return jsonify(ResponseHttp),ResponseHttp['code']
@app.route("/otp/save",methods=["POST"])
def otp_save():
    token=request.json['token']
    ResponseHttp ={}
    try:
        if (user.cek_user_token(token)):
            idDevice=''
            updateBy=''

            ResponseHttp['data'] = wifi.otp_save(token,request.json)
            if ResponseHttp['data'] != 'error':
                ResponseHttp['code'] = 200
                ResponseHttp['status']='success'
            else :
                ResponseHttp['status']='failed'
                ResponseHttp['code'] = 200
                ResponseHttp['message'] = 'failed'

        else:
            ResponseHttp['code'] = 401
            ResponseHttp['message'] = 'Not Authorized'
            ResponseHttp['data'] = []
    except Exception as e:
        logger.save('error on history_save() => ', e)
        ResponseHttp['code'] = 400
        ResponseHttp['message'] = 'Bad Request'
        ResponseHttp['data'] = []
    return jsonify(ResponseHttp),ResponseHttp['code']
@app.route("/otp/validate",methods=["POST"])
def otp_validate():

    token=request.json['token']
    try:
        if (user.cek_user_token(token)):
            idDevice=''
            updateBy=''

            ResponseHttp['data'] = wifi.otp_validate(token,request.json)
            if ResponseHttp['data'] == True:
                ResponseHttp['code'] = 200
                ResponseHttp['status']='success'
            else :
                ResponseHttp['status']='failed'
                ResponseHttp['code'] = 200
                ResponseHttp['message'] = 'failed'

        else:
            ResponseHttp['code'] = 401
            ResponseHttp['message'] = 'Not Authorized'
            ResponseHttp['data'] = []
    except Exception as e:
        logger.save('error on history_save() => ', e)
        ResponseHttp['code'] = 400
        ResponseHttp['message'] = 'Bad Request'
        ResponseHttp['data'] = []
    return jsonify(ResponseHttp),ResponseHttp['code']

@app.route("/helmed/save",methods=["POST"])
def helmed_save():
    token=request.json['token']
    try:
        if (user.cek_user_token(token)):
            idDevice=''
            updateBy=''

            ResponseHttp['data'] = helmed.helmed_save(request.json)
            if ResponseHttp['data'] != 'error':
                ResponseHttp['code'] = 200
                ResponseHttp['status']='success'
            else :
                ResponseHttp['status']='failed'
                ResponseHttp['code'] = 500
                ResponseHttp['message'] = 'failed'

        else:
            ResponseHttp['code'] = 401
            ResponseHttp['message'] = 'Not Authorized'
            ResponseHttp['data'] = []
    except Exception as e:
        logger.save('error on history_save() => ', e)
        ResponseHttp['code'] = 400
        ResponseHttp['message'] = 'Bad Request'
        ResponseHttp['data'] = []
    return jsonify(ResponseHttp),ResponseHttp['code']

@app.route("/helmed/report/day",methods=["GET"])
def helmed_report_day():
    try:
        if (user.cek_user_token(request.headers['token'])):
            ResponseHttp['code'] = 200
            ResponseHttp['message'] = 'ok'
            page = request.args.get('page', default=1, type=int)
            limit = request.args.get('limit', default=10, type=int)
            idCar = request.headers['token'] #request.args.get('id_car', default='', type=str)
            ResponseHttp['data'] = helmed.helmed_list(page,limit)
        else:
            ResponseHttp['code'] = 401
            ResponseHttp['message'] = 'Not Authorized'
            ResponseHttp['data'] = []
    except Exception as e:
        logger.save('Failed helmed_list() ', str(e))
        ResponseHttp['code'] = 400
        ResponseHttp['message'] = 'Bad Request'
        ResponseHttp['data'] = []

    return jsonify(ResponseHttp),ResponseHttp['code']
@app.route("/helmed/rekap",methods=["GET"])
def helmed_rekap1():
    try:
        if (user.cek_user_token(request.headers['token'])):
            ResponseHttp['code'] = 200
            ResponseHttp['message'] = 'ok'
            ResponseHttp['data'] = helmed.helmed_rekap1()
        else:
            ResponseHttp['code'] = 401
            ResponseHttp['message'] = 'Not Authorized'
            ResponseHttp['data'] = []
    except Exception as e:
        logger.save('Failed helmed_list() ', str(e))
        ResponseHttp['code'] = 400
        ResponseHttp['message'] = 'Bad Request'
        ResponseHttp['data'] = []

    return json.dumps(ResponseHttp),ResponseHttp['code']
@app.route("/helmed/rekap2",methods=["GET"])
def helmed_rekap2():
    try:
        if (user.cek_user_token(request.headers['token'])):
            ResponseHttp['code'] = 200
            ResponseHttp['message'] = 'ok'
            ResponseHttp['data'] = helmed.helmed_rekap2()
        else:
            ResponseHttp['code'] = 401
            ResponseHttp['message'] = 'Not Authorized'
            ResponseHttp['data'] = []
    except Exception as e:
        logger.save('Failed helmed rekap2() ', str(e))
        ResponseHttp['code'] = 400
        ResponseHttp['message'] = 'Bad Request'
        ResponseHttp['data'] = []

    return jsonify(ResponseHttp),ResponseHttp['code']
@app.route("/helmed/rekap3",methods=["GET"])
def helmed_rekap3():
    try:
        if (user.cek_user_token(request.headers['token'])):
            ResponseHttp['code'] = 200
            ResponseHttp['message'] = 'ok'
            ResponseHttp['data'] = helmed.helmed_rekap3()
        else:
            ResponseHttp['code'] = 401
            ResponseHttp['message'] = 'Not Authorized'
            ResponseHttp['data'] = []
    except Exception as e:
        logger.save('Failed helmed rekap 3t() ', str(e))
        ResponseHttp['code'] = 400
        ResponseHttp['message'] = 'Bad Request'
        ResponseHttp['data'] = []

    return jsonify(ResponseHttp),ResponseHttp['code']

if __name__ == "__main__":
    app_dir = os.path.realpath(os.path.dirname(__file__))
    valuePort=setting.get_setting_value('Port')
    if valuePort > 0:
        PORT_SERVICE = valuePort
    user.init_user_admin()
    envSetting = os.environ['CAMERA_SYS_ENV']
    handler = RotatingFileHandler('camera-backend.log', maxBytes=10000, backupCount=1)
    handler.setLevel(logging.INFO)
    app.logger.addHandler(handler)
    app.run("0.0.0.0", port=PORT_SERVICE)


