from __future__ import print_function

#import numpy as np
import cv2
import requests
import os
import urllib3
import time
from datetime import datetime

envSetting = 'DEVELOPMENT'
if 'CAMERA_SYS_ENV' in os.environ:
    envSetting = os.environ['CAMERA_SYS_ENV']
envDurationSend = 300
#int(os.environ['DURATION_CAMERA_SYS_ENV'])
urlBackend ="http://localhost:7201"
#urlBackend="http://iot-backend-dev.promogo.com"
token1="3ab4cb12-6f9d-47cf-8d9b-de754c554fb7"
token2="3ab4cb12-6f9d-47cf-8d9b-de754c554fb8"
token3="3ab4cb12-6f9d-47cf-8d9b-de754c554fb9"
token4="3ab4cb12-6f9d-47cf-8d9b-de754c554fc0"
directory = 'polaUrl/'
senData = True
polaFromUrl = True
hematMemory = False
if envSetting!="LOCALHOST":
    urlBackend="http://iot-backend-dev.promogo.com"
    token1="3ab4cb12-6f9d-47cf-8d9b-de754c554fb7"
    token2="3ab4cb12-6f9d-47cf-8d9b-de754c554fb8"
    token3="3ab4cb12-6f9d-47cf-8d9b-de754c554fb9"
    token4 = "3ab4cb12-6f9d-47cf-8d9b-de754c554fc0"
    directory = '/home/pi/iot/polaUrl/'
usedToken = token1
dataPull =[]
serverStatus = False
flag=0
img=0
videoParameter=0
video_src=0
cascade_1=0
cascade_2=0
cascade_3=0
cascade_4=0
args=0
motor_cascade=0
car_cascade=0
plate_cascade=0
human_cascade=0
cap=0

def send_object_data(data,token):
    #logger_save('iot', "send_object_data : "+ str(data),usedToken)
    #print('send_object_data, data ->'+str(data))
    #print('send_object_data, token ->' + token)
    try:
        if senData :
            r =requests.post(urlBackend+"/history/save",headers={'Content-Type':'application/json','token':token}, json={"mode":0,"list_data":data})
            response= r.json()
            print(str(response))
            if response['status'] == 1 : #success
                global dataPull
                dataPull=[]
            return response
        else:
            return {}
    except:
        return {}

def send_object_image(fileName,token):
    #logger_save('iot', "send_object_image : "+ str(fileName),token)
    #print('send_object, token -> '+token)
    #print('send_object, fileName -> ' + fileName)
    try:
        if senData :
            files = {'image': open(fileName, 'rb')}
            r =requests.post(urlBackend+"/image/save",headers={'token':token}, files=files, data={'mode':0,'camera':videoParameter})
            response= r.json()
            print(str(response))

            if response['status'] == 1 : #success
                os.remove(fileName)
            return response
        else:
            return {}
    except:
        return {}

def cek_server_status(token):

    result = False
    #global serverStatus
    try:
        r = requests.post(urlBackend + "/server/status", headers={'Content-Type': 'application/json', 'token': token},
                          json={})
        response = r.json()
        if response['message']=='ok':
            result=True
        else:
            result=False
        #print(str(response))
    except:
        result = False
    return result
def logger_save(endPoint,message,token):
    dataSend={}
    dataSend['engine']='detect'
    dataSend['endPoint']=endPoint
    dataSend['message']=message
    try:
        if senData :
            r =requests.post(urlBackend+"/logger/save",headers={'Content-Type':'application/json','token':token}, json=dataSend)
            response= r.json()
            return response
        else:
            return {}
    except:
        return {}
def path():
    return os.path.dirname(__file__)
def download_pola():
    logger_save('iot', "download_pola() : ",usedToken )
    polas=["cars.xml","motors.xml","plates.xml","humans.xml"]
    polasTemp = ["carsTemp.xml", "motorsTemp.xml", "platesTemp.xml", "humansTemp.xml"]
    urllib3.disable_warnings()

    if not os.path.exists(directory):
        os.makedirs(directory)
    jalankanSampaiServerOk=True
    while jalankanSampaiServerOk:
        #print('cek server')
        if cek_server_status(usedToken):

            jalankanSampaiServerOk=False
            try:
                for j in range(0, len(polas)):
                    with urllib3.PoolManager() as http:
                        r = http.request('GET', urlBackend + "/static/pola/" + str(polas[j]), preload_content=False)
                        with open(directory + str(polas[j]), 'wb') as out:
                            while True:
                                data = r.read()
                                if not data:
                                    break
                                out.write(data)

                        r.release_conn()
                return 'ok'
            except Exception as e:
                logger_save('download_pola', str(e),usedToken)
                return 'false'
def inisialisasi():
    global flag, img ,videoParameter, usedToken, video_src,cascade_1,cascade_2,cascade_3,cascade_4,args,motor_cascade,car_cascade,plate_cascade,human_cascade, cap
    logger_save('iot', "inisialisasi() ", usedToken)
    if polaFromUrl==True:

        video_src = 0
        if videoParameter=='usb1':
            usedToken = token2
            video_src = 1
        if videoParameter=='usb2':
            usedToken = token3
            video_src = 2
        if videoParameter=='usb3':
            usedToken = token4
            video_src = 3
        if videoParameter=='usb0':
            usedToken = token1
            video_src = 0
        else:
            video_src = videoParameter #"source/source06.mp4"
        logger_save('iot', "videoParameter usedToken : "+ videoParameter,usedToken )
        download_pola()
        cascade_1 =directory+"motors.xml"
        cascade_2 =directory+"cars.xml"
        cascade_3 =directory+"plates.xml"
        cascade_4 = directory+"humans.xml"

    else:

        cascade_1 = "uji_06/cascade/cascade.xml"
        cascade_2 = "uji_06_mobil/cascade/cascade.xml"
        cascade_3 = "pola/plates.xml"
        cascade_4 = "pola/humans.xml"
        video_src = "source/source06.mp4"
    motor_cascade = cv2.CascadeClassifier(cascade_1)
    car_cascade = cv2.CascadeClassifier(cascade_2)
    plate_cascade = cv2.CascadeClassifier(cascade_3)
    human_cascade = cv2.CascadeClassifier(cascade_4)
    #print(videoParameter)
    videoBelumSiap = True
    while videoBelumSiap:
        try:
            cap = cv2.VideoCapture(video_src)
            flag, img = cap.read()
            height, width, c = img.shape
            videoBelumSiap=False
            logger_save('VideoCapture', "Video sudah siap.", usedToken)
        except:
            logger_save('VideoCapture', "Video belum siap.", usedToken)
            #exit()
    flag, img = cap.read()

if __name__ == '__main__':
    import sys, getopt

    opts, args = getopt.getopt(sys.argv[1:], "", ["video"])
    videoParameter=args[0]
    print('videoParameter => '+videoParameter)
    inisialisasi()


    flag, img = cap.read()
    height, width, c = img.shape
    #out = cv2.VideoWriter('output2018-08-06-C.avi', -1, 1, (width, height))

    paused = False
    step = True
    font = cv2.FONT_HERSHEY_SIMPLEX
    bottomLeftCornerOfText = (10, 40)
    fontScale = 1
    fontColor = (100, 10, 155)
    lineType = 2
    timeSend =0
    arrayDataCars=[]
    arrayDataMotors = []
    arrayDataPlates = []
    arrayDataHumans = []
    tic = time.clock()
    k=0
    while True:
        if not paused or step:
            try:
                flag, img = cap.read()
                #out.write(img)
                #if img == None: break
                #if img.any(): break

                height, width, c = img.shape
                gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
                cars = car_cascade.detectMultiScale(gray, 1.2, 5)
                motors = motor_cascade.detectMultiScale(gray, 1.2, 5)
                plates = plate_cascade.detectMultiScale(gray, 1.2, 5)
                humans = human_cascade.detectMultiScale(gray, 1.2, 5)
                #if not hematMemory:
                #cv2.imshow('edge', img)
                duration = time.clock() - tic
                if duration > envDurationSend:
                    for (x,y,w,h) in cars:
                        cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,255),2)
                    for (x,y,w,h) in motors:
                        cv2.rectangle(img,(x,y),(x+w,y+h),(0,200,255),2)
                    for (x, y, w, h) in plates:
                        cv2.rectangle(img, (x, y), (x + w, y + h), (0, 50, 255), 2)
                    for (x, y, w, h) in humans:
                        cv2.rectangle(img, (x, y), (x + w, y + h), (0, 250, 255), 2)
                    cv2.putText(img, 'Mobil : ' +str(len(cars)),
                                                bottomLeftCornerOfText,
                                                font,
                                                fontScale,
                                                fontColor,
                                                lineType)
                    cv2.putText(img, 'Motor : ' +str(len(motors)),
                                                (10,70),
                                                font,
                                                fontScale,
                                                fontColor,
                                                lineType)
                    cv2.putText(img, 'Plates : '+str(len(plates)),
                                                (10,100),
                                                font,
                                                fontScale,
                                                fontColor,
                                                lineType)
                    cv2.putText(img, 'Humans : '+str(len(humans)),
                                                (10,130),
                                                font,
                                                fontScale,
                                                fontColor,
                                                lineType)
                    #cv2.imshow('edge', img)
                    k=k+1
                    imageSaved=directory+"img-"+str(datetime.now())+".jpg"
                    cv2.imwrite(imageSaved, img)
                    send_object_image(imageSaved,usedToken)
                #out.write(img)

                timeSend=timeSend+1

                arrayDataCars.append(len(cars))
                arrayDataMotors.append(len(motors))
                arrayDataPlates.append(len(plates))
                arrayDataHumans.append(len(humans))
                #if timeSend >6:
                #print('arrayDataCars : '+str(arrayDataCars))
                if duration>envDurationSend:

                    objectData={"cars":arrayDataCars[arrayDataCars.index(max(arrayDataCars))],
                                "motors":arrayDataMotors[arrayDataMotors.index(max(arrayDataMotors))],
                                "humans":arrayDataMotors[arrayDataHumans.index(max(arrayDataHumans))],
                                "plates": arrayDataMotors[arrayDataPlates.index(max(arrayDataPlates))],
                                "lat":0,
                                "long":0,
                                "created_date" : str(datetime.now()) }
                    dataPull.append(objectData)
                    r=send_object_data(dataPull,usedToken)
                    arrayDataCars = []
                    arrayDataMotors = []
                    arrayDataPlates = []
                    arrayDataHumans = []
                    timeSend=0
                    tic = time.clock()
            except:
                inisialisasi()

        step = False
        ch = cv2.waitKey(5)
        if ch == 13:
            step = True
        if ch == 32:
            paused = not paused
        if ch == 27:
            out.release()
            break
    #out.release()
    cv2.destroyAllWindows()