from pymongo import MongoClient
from bson.objectid import ObjectId
from datetime import datetime
import os
hostMongo='localhost'
envSetting = os.environ['CAMERA_SYS_ENV']
if envSetting=='DEVELOPMENT':
    hostMongo='172.27.3.11'

mongoClient = MongoClient(hostMongo, 27017)
db = mongoClient.iot
USER_TBL = db.user
ROLE_TBL = db.role

def init_user_admin():
    tempTbl = list(USER_TBL.find({'user': 'admin'}))
    result={}
    if len(tempTbl) == 0:
        tempObject={}
        tempObject['user']='admin'
        tempObject['pass']='admin'
        tempObject['nama']='Admin'
        tempObject['email'] = 'admin@promogo.id'
        tempObject['role']=10
        tempObject['token']='3ab4cb12-6f9d-47cf-8d9b-de754c554fb9'
        tempObject['created_at'] = datetime.now()
        #str(uuid4())
        USER_TBL.insert(tempObject)
        del tempObject['_id']
        result=tempObject
    else:
        result=tempTbl[0]
        del result['_id']
    return result

def user_list():
    result = []
    try:
        listUser = list(USER_TBL.find({}))
        for data in listUser:
            tempRoles = []
            for i in range(0, len(data['roles'])):
                tempRoles.append(str(data['roles'][i]))

            tempObject = data
            tempObject['_id']=str(tempObject['_id'])
            del tempObject['roles']

            tempObject['roles']= tempRoles
            result.append(tempObject)

        return result

    except:
        return result

def user_list_query(page,limit,searchText):
    result = []
    try:
        listUser = list(USER_TBL.find({'email':{'$regex' : searchText, '$options' : 'i'}}).skip((page-1)*limit).limit(limit))
        for data in listUser:
            tempRoles = []
            for i in range(0, len(data['roles'])):
                tempRoles.append(str(data['roles'][i]))

            tempObject = data
            tempObject['_id']=str(tempObject['_id'])
            del tempObject['roles']

            tempObject['roles']= tempRoles
            result.append(tempObject)

        return result
    except:
        return result

def cek_user_token(token):
    result = False
    try:
        tempTbl = list(USER_TBL.find({'token': token}))
        if len(tempTbl) > 0:
            result= True
    except:
        result=False

    return result

def role_list():
    result = []
    try:
        listRole = list(ROLE_TBL.find({}))
        for i in range(0, len(listRole)):
            result.append({'_id':str(listRole[i]['_id']), 'name':listRole[i]['name']})
        return result

    except:
        return result

def user_view(id):
    result = {}
    try:
        data = USER_TBL.find_one({"_id": ObjectId(id)})
        if (data):
            tempRoles = []
            for i in range(0, len(data['roles'])):
                tempRoles.append(str(data['roles'][i]))

            result = data
            result['_id'] = str(data['_id'])
            del result['roles']

            result['roles'] = tempRoles

        return result

    except:
        return result
def user_delete(id):
    try:
        USER_TBL.remove({"_id": ObjectId(id)})
        return True
    except:
        return False

def user_update(id,email,name,token,role,active):
    object = {'email':email, 'name':name, 'token':token, 'roles':[role], 'active':active}
    try:
        USER_TBL.update_one({"_id": ObjectId(id)}, {"$set": object})
        return True

    except:
        return False
