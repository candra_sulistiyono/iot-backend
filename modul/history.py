from pymongo import MongoClient, DESCENDING
from datetime import datetime, timedelta
from bson.objectid import ObjectId
from modul import logger, setting
import os
hostMongo='localhost'
mongoClient = MongoClient(hostMongo, 27017)
db = mongoClient.iot
envSetting = os.environ['CAMERA_SYS_ENV']
if envSetting=='DEVELOPMENT':
    hostMongo='172.27.3.11'
    mongoClient = MongoClient(hostMongo, 27017)
    db = mongoClient.iot

HISTORY_TBL = db.history


def history_list(page,limit,idCar):
    result = {}
    try:
        listData = []
        if idCar=="":
            collectionData = HISTORY_TBL.find({}).sort([('created_date', -1)]).skip((page-1)*limit).limit(limit)
        else:
            collectionData = HISTORY_TBL.find({'id_car': {'$regex': idCar, '$options': 'i'}}).sort([('created_date', -1)]).skip((page-1)*limit).limit(limit)
        #collectionData = HISTORY_TBL.find({'id_car':{'$regex': searchText, '$options' : 'i'}}).sort({'created_at':-1}).skip((page-1)*limit).limit(limit)
        result['total'] = collectionData.count()
        i=0
        for data in collectionData:
            data['_id'] = str(data['_id'])
            if 'plate' not in data:
                data['plate']=0
            if 'created_date' in data:
                data['created_date'] = str(data['created_date'])
            if 'updated_date' in data:
                data['updated_date'] = str(data['updated_date'])
            i=i+1
            listData.append(data)
        result['count'] = i
        result['data']=listData
        return result

    except Exception as e:
        logger.save('Failed history_list() ', str(e))
        return result

#def history_save(mode,id,idCar,cars,motors,plates,humans,lat,long,update_by):
def history_save(mode, id, idCar, listData, update_by):
    try:
        recordSaved=0
        for data in listData:
            object= {}
            object["id_car"] = idCar
            object["cars"] = data['cars']
            object["motors"] = data['motors']
            object["plates"] = data['plates']
            object["humans"] = data['humans']
            object["lat"] = data['lat']
            object["long"] = data['long']
            object["update_by"] = update_by
            if mode == 1: #Update
                object['updated_date'] = datetime.now()
                HISTORY_TBL.update_one({"_id": ObjectId(id)}, {"$set": object})
            else:
                #object['created_date'] = datetime.now()
                object['created_date'] = data['created_date']
                object['updated_date'] = datetime.now()
                HISTORY_TBL.insert(object)
                recordSaved=recordSaved+1
                #object['_id']=str(object['_id'])
        return str(recordSaved)+' saved'

    except Exception as e:
        logger.save('error on history_save => ', str(e))
        return 'error'

def history_save2(mode, id, idCar, listData, update_by):
    try:
        deltaDays = setting.get_setting_value('deltaDays')
        deltaHours = setting.get_setting_value('deltaHours')
        deltaMinutes = setting.get_setting_value('deltaMinutes')
        recordSaved=0
        for data in listData:
            object= {}
            object["id_car"] = idCar
            object["cars"] = data['cars']
            object["motors"] = data['motors']
            object["plates"] = data['plates']
            object["humans"] = data['humans']
            object["lat"] = data['lat']
            object["long"] = data['long']
            object["update_by"] = update_by
            if mode == 1: #Update
                object['updated_date'] = datetime.now()
                HISTORY_TBL.update_one({"_id": ObjectId(id)}, {"$set": object})
            else:
                #object['created_date'] = datetime.now()
                object['created_date'] = str(datetime.now() - timedelta(days=deltaDays,hours=deltaHours, minutes=deltaMinutes))
                object['updated_date'] = datetime.now() - timedelta(days=deltaDays,hours=deltaHours, minutes=deltaMinutes)
                HISTORY_TBL.insert(object)
                recordSaved=recordSaved+1
                #object['_id']=str(object['_id'])
        return str(recordSaved)+' saved'

    except Exception as e:
        logger.save('error on history_save => ', str(e))
        return 'error'




def history_clear():
    try:
        HISTORY_TBL.remove({})
        return True
    except Exception as e:
        logger.save('error on e =.',str(e))
        return False


def history_delete(id):
    try:
        HISTORY_TBL.remove({"_id": ObjectId(id)})
        return True
    except Exception as e:
        logger.save('error history_delete ', str(e))
        return False


def history_view(id):
    try:
        result = {}
        listData = list(HISTORY_TBL.find({"_id": ObjectId(id)}))
        if len(listData) > 0:
            listData[0]['_id']=str(listData[0]['_id'])
            listData[0]['created_date'] = str(listData[0]['created_date'])
            result =listData[0]
        return result
    except:
        return {}

def history_rekap1():
    try:
        result = list(HISTORY_TBL.aggregate([{"$group":{"_id":"null",
                                                     "cars": {"$sum": "$cars"},
                                                     "motors": {"$sum": "$motors"},
                                                     "humans": {"$sum": "$humans"},
                                                     "plates": {"$sum": "$plates"},}}]))

        return result[0]

    except Exception as e:
        logger.save('Failed history_rekap1() ', str(e))
        return []

def history_rekap2():
    try:
        result = list(HISTORY_TBL.aggregate([{"$group":{"_id":{ "$week": "$updated_date" },
                                                        "cars": {"$sum": "$cars"},
                                                        "motors": {"$sum": "$motors"},
                                                        "humans": {"$sum": "$humans"},
                                                        "plates": {"$sum": "$plates"},}}]))

        return result

    except Exception as e:
        print('Failed history_rekap2() ', str(e))
        logger.save('Failed history_rekap2() ', str(e))
        return []

