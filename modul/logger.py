from pymongo import MongoClient
import os
hostMongo='localhost'
envSetting = os.environ['CAMERA_SYS_ENV']
if envSetting=='DEVELOPMENT':
    hostMongo='172.27.3.11'

mongoClient = MongoClient(hostMongo, 27017)
db = mongoClient.iot
LOGGER_TBL = db.logger
PORT_SERVICE =7200
from datetime import datetime

def logger_list_query(page,limit,searchText):
    try:
        result = []
        tblResult = list(LOGGER_TBL.find({'end_point':{'$regex' : searchText, '$options' : 'i'}}).sort('created_date',-1).skip((page-1)*limit).limit(limit))

        for data in tblResult:
            del data['_id']
            tempObject = data
            result.append(tempObject)
        return result

    except Exception as e:
        print('Failed logger_list_query() ' + str(e))
        return result

def save(endPoint,message):
    object = {}
    object['created_date'] = datetime.now()
    object['end_point'] = endPoint
    object["message"] = message
    id=LOGGER_TBL.insert(object)
    return str(id)
def save_from_other(engine,endPoint,message):
    object = {}
    object['created_date'] = datetime.now()
    object['engine'] = engine
    object['end_point'] = endPoint
    object["message"] = str(message)
    id=LOGGER_TBL.insert(object)
    return 'Success '+str(id)
