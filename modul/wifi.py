from pymongo import MongoClient, DESCENDING
from datetime import datetime, timedelta
from bson.objectid import ObjectId
from modul import logger, setting
import os
hostMongo='localhost'
mongoClient = MongoClient(hostMongo, 27017)
db = mongoClient.iot
envSetting = os.environ['CAMERA_SYS_ENV']
if envSetting=='DEVELOPMENT':
    hostMongo='172.27.3.11'
    mongoClient = MongoClient(hostMongo, 27017)
    db = mongoClient.iot

WIFI_TBL = db.wifi
OTP_TBL = db.otp

def wifi_list(page,limit):
    result = {}
    try:
        listData = []
        collectionData = WIFI_TBL.find({}).sort([('created_date', -1)]).skip((page-1)*limit).limit(limit)
        result['total'] = collectionData.count()
        i=0
        for data in collectionData:
            data['_id'] = str(data['_id'])
            i=i+1
            listData.append(data)
        result['count'] = i
        result['data']=listData
        return result

    except Exception as e:
        logger.save('Failed history_list() ', str(e))
        return result

def wifi_save(idDevice, data):

    try:
        object = {}
        object["device_id"] = idDevice
        object["device_token"] = data["device_token"]
        object["medsos_id"] = data['medsos_id']
        object["medsos_fullname"] = data['medsos_fullname']
        object["medsos_type"] = data['medsos_type']
        object["browser"] = data['browser']
        object["browser_version"] = data['browser_version']
        object["os"] = data['os']
        object["os_version"] = data['os_version']
        object["engine"] = data['engine']
        object["engine_version"] = data['engine_version']
        object["profile"] = data['profile']


        if data["mode"] == 1:  # Update
            object['updated_date'] = datetime.now()
            object["updateBy"] = data['updateBy']
            WIFI_TBL.update_one({"_id": ObjectId(data["id"])}, {"$set": object})
        else:
            object["updateBy"] = ''
            object['created_date'] = datetime.now()
            object['updated_date'] = datetime.now()
            WIFI_TBL.insert(object)

        object["_id"]=str(object["_id"])
        return object

    except Exception as e:
        #logger.save('error on wifi_save => ', str(e))
        return 'error'




def history_clear():
    try:
        WIFI_TBL.remove({})
        return True
    except Exception as e:
        logger.save('error on e =.',str(e))
        return False


def history_delete(id):
    try:
        WIFI_TBL.remove({"_id": ObjectId(id)})
        return True
    except Exception as e:
        logger.save('error history_delete ', str(e))
        return False


def history_view(id):
    try:
        result = {}
        listData = list(WIFI_TBL.find({"_id": ObjectId(id)}))
        if len(listData) > 0:
            listData[0]['_id']=str(listData[0]['_id'])
            listData[0]['created_date'] = str(listData[0]['created_date'])
            result =listData[0]
        return result
    except:
        return {}

def wifi_rekap1():
    try:
        result = list(WIFI_TBL.aggregate([{"$group":{"_id":"$medsos_type","jumlah": {"$sum": 1},}}]))

        return result

    except Exception as e:
        logger.save('Failed wifi_rekap1() ', str(e))
        return []

def wifi_rekap2():
    try:
        result = list(WIFI_TBL.aggregate([{"$group": {"_id": { "$hour": "$created_date" },"medsos": { "$sum": 1 }, }}]))
        return result
    except Exception as e:
        logger.save('Failed wifi_rekap2() ', str(e))
        return []

def wifi_rekap3():
    try:
        result = {}
        result["instagram"] = list(WIFI_TBL.aggregate([{"$group": { "_id": { "$week": "$created_date" },
                                                       "medsos": { "$sum": {"$cond": { "if": { "$eq": [ "$medsos_type", "instagram" ] }, "then": 1, "else": 0 } }} }}]))
        result["gmail"] = list(WIFI_TBL.aggregate([{"$group": {"_id": {"$week": "$created_date"},
                                                                   "medsos": {"$sum": {"$cond": {
                                                                       "if": {"$eq": ["$medsos_type", "gmail"]},
                                                                       "then": 1, "else": 0}}}}}]))
        result["twitter"] = list(WIFI_TBL.aggregate([{"$group": {"_id": {"$week": "$created_date"},
                                                               "medsos": {"$sum": {"$cond": {
                                                                   "if": {"$eq": ["$medsos_type", "twitter"]},
                                                                   "then": 1, "else": 0}}}}}]))
        result["otp"] = list(WIFI_TBL.aggregate([{"$group": {"_id": {"$week": "$created_date"},
                                                               "medsos": {"$sum": {"$cond": {
                                                                   "if": {"$eq": ["$medsos_type", "OTP"]},
                                                                   "then": 1, "else": 0}}}}}]))
        result["facebook"] = list(WIFI_TBL.aggregate([{"$group": {"_id": {"$week": "$created_date"},
                                                               "medsos": {"$sum": {"$cond": {
                                                                   "if": {"$eq": ["$medsos_type", "facebook"]},
                                                                   "then": 1, "else": 0}}}}}]))
        return result
    except Exception as e:
        logger.save('Failed wifi_rekap2() ', str(e))
        return []

def otp_save(idDevice, data):

    try:
        OTP_TBL.update_many({"mobile_number": data["mobile_number"]}, {"$set": {"expired":1}})
        object = {}
        object["device_id"] = idDevice
        object["mobile_number"] = data["mobile_number"]
        object["otp_number"] = data['otp_number']
        object["expired"] = 0

        if data["mode"] == 1:  # Update
            object['updated_date'] = datetime.now()
            object["updateBy"] = data['updateBy']
            OTP_TBL.update_one({"_id": ObjectId(data["id"])}, {"$set": object})
        else:
            object["updateBy"] = ''
            object['created_date'] = datetime.now()
            object['updated_date'] = datetime.now()
            OTP_TBL.insert(object)

        object["_id"]=str(object["_id"])
        return object

    except Exception as e:
        return 'error'

def otp_validate(idDevice, data):

    try:
        listData = list(OTP_TBL.find({"mobile_number": data["mobile_number"],"otp_number": data["otp_number"],"expired":0 }))
        if len(listData) > 0:
            OTP_TBL.update_many({"mobile_number": data["mobile_number"]}, {"$set": {"expired": 1}})
            return True
        else:
            return False
    except Exception as e:
        return False
