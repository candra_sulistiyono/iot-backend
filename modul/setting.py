from pymongo import MongoClient
import os
hostMongo='localhost'
envSetting = os.environ['CAMERA_SYS_ENV']
if envSetting=='DEVELOPMENT':
    hostMongo='172.27.3.11'

mongoClient = MongoClient(hostMongo, 27017)
db = mongoClient.iot
SETTING_TBL = db.setting
PORT_SERVICE =7200


def setting_setup():
    listResult=[]
    try:
        SETTING_TBL.remove()
        settingObject = {}
        settingObject['code']='ScoreMinTO'
        settingObject['description'] = 'Score Minimal to Take Over Admin'
        settingObject['value']=25
        SETTING_TBL.insert(settingObject)
        del settingObject['_id']
        listResult.append(settingObject)

        settingObject = {}
        settingObject['code'] = 'Port'
        settingObject['description'] = 'Port Service NLP-Engine'
        settingObject['value'] = PORT_SERVICE
        SETTING_TBL.insert(settingObject)
        del settingObject['_id']
        listResult.append(settingObject)

        settingObject = {}
        settingObject['code'] = 'CountPert'
        settingObject['description'] = 'Counter Id Question'
        settingObject['value'] = 0
        SETTING_TBL.insert(settingObject)
        del settingObject['_id']
        listResult.append(settingObject)

        settingObject = {}
        settingObject['code'] = 'CountJaw'
        settingObject['description'] = 'Counter Id Answer'
        settingObject['value'] = 0
        SETTING_TBL.insert(settingObject)
        del settingObject['_id']
        listResult.append(settingObject)

        settingObject = {}
        settingObject['code'] = 'CountStopWord'
        settingObject['description'] = 'Counter Id Stop Word'
        settingObject['value'] = 0
        SETTING_TBL.insert(settingObject)
        del settingObject['_id']
        listResult.append(settingObject)

        settingObject = {}
        settingObject['code'] = 'CountIntent'
        settingObject['description'] = 'Counter Id Intent'
        settingObject['value'] = 0
        SETTING_TBL.insert(settingObject)
        del settingObject['_id']
        listResult.append(settingObject)

        settingObject = {}
        settingObject['code'] = 'CountVarian'
        settingObject['description'] = 'Counter Id Varian'
        settingObject['value'] = 0
        SETTING_TBL.insert(settingObject)
        del settingObject['_id']
        listResult.append(settingObject)

        settingObject = {}
        settingObject['code'] = 'CountDict'
        settingObject['description'] = 'Counter Id Dictionary'
        settingObject['value'] = 0
        SETTING_TBL.insert(settingObject)
        del settingObject['_id']
        listResult.append(settingObject)
        #return listResult
        return 'Setup Setting finished ' + str(len(listResult)) + ' record'
    except:
        return 'Setup Setting finished ' + str(len(listResult)) + ' record'


def setting_update(code,value):
    settingObject = {}
    settingObject['code'] = code
    dataSetting = list(SETTING_TBL.find(settingObject))
    if len(dataSetting)>0:
        id = SETTING_TBL.update({'code': code}, {'$set': {'value': value}})
    else:
        #settingObject['description'] = dataSetting[0]['description']
        settingObject['description'] = code
        settingObject['value'] = value

        id= SETTING_TBL.insert(settingObject)
    return str(id)
def setting_update_object(settingObject):
    tempList = list(SETTING_TBL.find({'code': settingObject['code']}))
    if len(tempList) == 0:
        SETTING_TBL.insert(settingObject)
    else:
        SETTING_TBL.update({'code': settingObject['code']}, {'$set': settingObject})
    return settingObject['value']


def get_setting_counter(code):
    settingObject = {}
    settingObject['code'] = code
    dataSetting = list(SETTING_TBL.find(settingObject))
    if len(dataSetting)>0:
        value= int(dataSetting[0]['value'] + 1 )
        settingObject['value']=value
        SETTING_TBL.update({'code': code}, {'$set': settingObject})
    else:
        value = 1
        settingObject['value'] =  value
        SETTING_TBL.insert({'code': code}, {'$set': settingObject})
    #settingObject['description'] = dataSetting[0]['description']
    #value = dataSetting[0]['value'] + 1
    #settingObject['value'] = value
    #SETTING_TBL.update({'code': code}, {'$set': settingObject})
    return value

def get_setting_value(code):
    settingObject = {}
    settingObject['code'] = code
    dataSetting = list(SETTING_TBL.find(settingObject))
    value=0
    if len(dataSetting)>0:
        value = dataSetting[0]['value']
    else:
        settingObject['value']=0
        SETTING_TBL.insert(settingObject)
    return value


def init_listSetting():
    listSetting=[]
    listTemp =list(SETTING_TBL.find({}))
    for data in listTemp:
        del data['_id']
        listSetting.append(data)
    return listSetting
