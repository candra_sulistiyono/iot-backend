from pymongo import MongoClient, DESCENDING
from datetime import datetime, timedelta
from bson.objectid import ObjectId
from modul import logger
import os
hostMongo='localhost'
mongoClient = MongoClient(hostMongo, 27017)
db = mongoClient.iot
envSetting = os.environ['CAMERA_SYS_ENV']
if envSetting=='DEVELOPMENT':
    hostMongo='172.27.3.11'
    mongoClient = MongoClient(hostMongo, 27017)
    db = mongoClient.iot

IMAGES_TBL = db.images


def images_list(page,limit,idCar):
    result = {}
    try:
        listData = []
        if idCar=="":
            collectionData = IMAGES_TBL.find({}).sort([('created_date', -1)]).skip((page-1)*limit).limit(limit)
        else:
            collectionData = IMAGES_TBL.find({'id_car': {'$regex': idCar, '$options': 'i'}}).sort([('created_date', -1)]).skip((page-1)*limit).limit(limit)
        #collectionData = IMAGES_TBL.find({'id_car':{'$regex': searchText, '$options' : 'i'}}).sort({'created_at':-1}).skip((page-1)*limit).limit(limit)
        result['total'] = collectionData.count()
        i=0
        for data in collectionData:
            data['_id'] = str(data['_id'])
            i=i+1
            listData.append(data)
        result['count'] = i
        result['data']=listData
        return result

    except Exception as e:
        logger.save('Failed images_list() ', str(e))
        return result

#def images_save(mode,id,idCar,cars,motors,plates,humans,lat,long,update_by):
def images_save(mode, id, idCar,camera,fileName,fileLocation, update_by):
    try:
        recordSaved=0

        object = {}
        object["id_car"] = idCar
        object["camera"] = camera
        object["file_name"] = fileName
        object["file_location"] = fileLocation
        object["update_by"] = update_by
        if mode == 1:  # Update
            object['updated_date'] = datetime.now()
            IMAGES_TBL.update_one({"_id": ObjectId(id)}, {"$set": object})
        else:
            # object['created_date'] = datetime.now()
            object['created_date'] = datetime.now()
            object['updated_date'] = datetime.now()
            IMAGES_TBL.insert(object)
            # object['_id']=str(object['_id'])
        return str(recordSaved)+' saved'

    except Exception as e:
        logger.save('error on images_save => ', str(e))
        return 'error'

def images_save2(mode, id, idCar,camera,fileName,fileLocation, update_by):
    try:
        deltaDays = setting.get_setting_value('deltaDays')
        deltaHours = setting.get_setting_value('deltaHours')
        deltaMinutes = setting.get_setting_value('deltaMinutes')
        recordSaved=0

        object = {}
        object["id_car"] = idCar
        object["camera"] = camera
        object["file_name"] = fileName
        object["file_location"] = fileLocation
        object["update_by"] = update_by
        if mode == 1:  # Update
            object['updated_date'] = datetime.now()
            IMAGES_TBL.update_one({"_id": ObjectId(id)}, {"$set": object})
        else:
            # object['created_date'] = datetime.now()
            object['created_date'] = datetime.now() - timedelta(days=deltaDays,hours=deltaHours, minutes=deltaMinutes)
            object['updated_date'] = datetime.now() - timedelta(days=deltaDays,hours=deltaHours, minutes=deltaMinutes)
            IMAGES_TBL.insert(object)
            # object['_id']=str(object['_id'])
        return str(recordSaved)+' saved'

    except Exception as e:
        logger.save('error on images_save => ', str(e))
        return 'error'



def images_clear():
    try:
        IMAGES_TBL.remove({})
        return True
    except Exception as e:
        logger.save('error on e =.',str(e))
        return False


def images_delete(id):
    try:
        IMAGES_TBL.remove({"_id": ObjectId(id)})
        return True
    except Exception as e:
        logger.save('error images_delete ', str(e))
        return False


def images_view(id):
    try:
        result = {}
        listData = list(IMAGES_TBL.find({"_id": ObjectId(id)}))
        if len(listData) > 0:
            listData[0]['_id']=str(listData[0]['_id'])
            listData[0]['created_date'] = str(listData[0]['created_date'])
            result =listData[0]
        return result
    except:
        return {}


