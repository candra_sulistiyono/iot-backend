from pymongo import MongoClient, DESCENDING
from datetime import datetime, timedelta
from bson.objectid import ObjectId
from modul import logger, setting
import os
hostMongo='localhost'
mongoClient = MongoClient(hostMongo, 27017)
db = mongoClient.iot
envSetting = os.environ['CAMERA_SYS_ENV']
if envSetting=='DEVELOPMENT':
    hostMongo='172.27.3.11'
    mongoClient = MongoClient(hostMongo, 27017)
    db = mongoClient.iot

HELMED_TBL = db.helmed
OTP_TBL = db.otp

def helmed_list(page,limit):
    result = {}
    try:
        listData = []
        collectionData = HELMED_TBL.find({}).sort([('created_date', -1)]).skip((page-1)*limit).limit(limit)
        result['total'] = collectionData.count()
        i=0
        for data in collectionData:
            data['_id'] = str(data['_id'])
            i=i+1
            listData.append(data)
        result['count'] = i
        result['data']=listData
        return result

    except Exception as e:
        logger.save('Failed history_list() ', str(e))
        return result

def helmed_save( data):

    try:
        object = {}
        object["driver_id"] = data["driver"]
        object["age"] = data["age"]
        object["gender"] = data['gender']
        object["loc"] = data['loc']

        object["browser"] = data['browser']
        object["browser_version"] = data['browser_version']
        object["os"] = data['os']
        object["os_version"] = data['os_version']
        object["engine"] = data['engine']
        object["engine_version"] = data['engine_version']


        if data["mode"] == 1:  # Update
            object['updated_date'] = datetime.now()
            object["updateBy"] = data['updateBy']
            HELMED_TBL.update_one({"_id": ObjectId(data["id"])}, {"$set": object})
        else:
            object["updateBy"] = ''
            object['created_date'] = datetime.now()
            object['updated_date'] = datetime.now()
            HELMED_TBL.insert(object)

        object["_id"]=str(object["_id"])
        return object

    except Exception as e:
        #logger.save('error on helmed_save => ', str(e))
        return 'error'




def history_clear():
    try:
        HELMED_TBL.remove({})
        return True
    except Exception as e:
        logger.save('error on e =.',str(e))
        return False


def history_delete(id):
    try:
        HELMED_TBL.remove({"_id": ObjectId(id)})
        return True
    except Exception as e:
        logger.save('error history_delete ', str(e))
        return False


def history_view(id):
    try:
        result = {}
        listData = list(HELMED_TBL.find({"_id": ObjectId(id)}))
        if len(listData) > 0:
            listData[0]['_id']=str(listData[0]['_id'])
            listData[0]['created_date'] = str(listData[0]['created_date'])
            result =listData[0]
        return result
    except:
        return {}

def helmed_rekap1():
    try:
        result = list(HELMED_TBL.aggregate([{"$group":{"_id":"$gender","jumlah": {"$sum": 1},}}]))

        return result

    except Exception as e:
        logger.save('Failed helmed_rekap1() ', str(e))
        return []

def helmed_rekap2():
    try:
        result = list(HELMED_TBL.aggregate([{"$group": {"_id": { "$hour": "$created_date" },"gender": { "$sum": 1 }, }}]))
        return result
    except Exception as e:
        logger.save('Failed helmed_rekap2() ', str(e))
        return []

def helmed_rekap3():
    try:
        result = {}
        result["male"] = list(HELMED_TBL.aggregate([{"$group": { "_id": { "$week": "$created_date" },
                                                       "gender": { "$sum": {"$cond": { "if": { "$eq": [ "gender", "male" ] }, "then": 1, "else": 0 } }} }}]))
        result["female"] = list(HELMED_TBL.aggregate([{"$group": {"_id": {"$week": "$created_date"},
                                                                   "gender": {"$sum": {"$cond": {
                                                                       "if": {"$eq": ["gender", "female"]},
                                                                       "then": 1, "else": 0}}}}}]))

        return result
    except Exception as e:
        logger.save('Failed helmed_rekap2() ', str(e))
        return []

